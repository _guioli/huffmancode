#define arquive "palavras.txt"
#include "lista.h"

plist create(int freq){
	plist aux=allocation_list();
	aux->freq=freq;
	aux->next = NULL;
	aux->left=NULL;
	aux->right=NULL;
	return aux;
}

plist allocation_list(){
plist aux = (plist) malloc(sizeof(list));
	if(aux== NULL){
		printf("This allocation is not possible\n");
		exit(1);
	}
return aux;
}

plist create_empty(){
	return NULL;
}

int empty_list(plist l){
	return l==NULL;
}

plist remove_list(plist l, int pos){
plist back,aux=l;
int n=1;
	if(empty_list(l)){
		printf("Invalid operation\n");
		exit(1);
	}
	if(pos==1){
		l =l->next;
		l->tam--;
	}
	else{
		while((n<pos)&&(aux!=NULL)){
			back=aux;
			aux=aux->next;
			n++;
		}
		back->next= aux->next;
		l->tam--;
	}
return l;
}


plist inserir(plist p, plist node, char c){
    plist atual, novo, anterior;
    int num = node->freq;
    
    novo = (list *) malloc(sizeof(list));
	
    atual = p;
    anterior = NULL;
    
    novo->freq = num;
    novo->symbol= c;
	novo->left=node->left;
	novo->right=node->right;

    if(atual == NULL){
        novo->next = NULL;
        p = novo;	
	return p;
    } else{
        while(atual != NULL && atual->freq <= num){
            anterior = atual;
            atual = atual->next;
        }
        
        novo->next = atual;
        p->tam++;
        if(anterior == NULL){
            p = novo;
	    return p;
        } else{
            anterior->next = novo;
	    return p;	
        }
    }

}

plist inserirnormal(plist p, int n, char c){
	plist aux = (list *) malloc(sizeof(list));
	aux->freq=n;
	aux->symbol=c;
	aux->next= p->next;
	p->next= aux;
	return p;
}

int menorelemento(plist l){
	plist novo = (list *) malloc(sizeof(list));
	novo=l;
	int menor = l->freq;
	int pos;
	int n=1;
	while(novo!=NULL){
		if(novo->freq<menor){	
			menor =novo->freq;
			pos=n;
		}
	novo=novo->next;
	n++;	
	}
return pos;
}
plist huffman_begin(plist l){
	//criação do vetor de frequencias	
	FILE *arq;
	char c;
	int num=0;
	int freq[256]={};
	//pnode create = allocation();
	//pnode* dad1 = &create;	
	plist insertion = create(0);
	if ((arq=fopen (arquive,"r")) != NULL) {
		while( (c=fgetc(arq)) !=EOF){	
			freq[c]++;
		}
	fclose(arq);
	}
	freq[10]--;// o caracter de numero 10 na tabela sempre ocorre 1 vez mesmo que inexistente no texto
	for(int i=0; i<255; i++){
		if(freq[i]!=0){
			insertion->freq=freq[i];
			inserir(l, insertion, i);
		}
	}	
	plist sum = create(0);
	int n=0;
	plist aux = l;
	while(l->next!=NULL){
		if(n==0){
		l=l->next;
		n=1;
		}
		aux = l;
		sum = create(0);
		sum->freq = aux->freq + aux->next->freq;
		sum->left= aux;
		sum->right = (aux->next);
		l= inserir(l,sum,-1);
		l = remove_list(l,1);
		l = remove_list(l,1);
		printf("SUM %d lson %d rson %d\n",sum->freq,sum->left->freq,sum->right->freq);
		print_list(l);	

	}	
//l=correction(l);
print_arv1(l);
		

}

void print_list(plist l){
plist aux = create(0);
aux=l;
	while(aux!=NULL){
	printf("%d ", aux->freq);		
	aux=aux->next;
	}
printf("\n");
}

void print_arv1(plist a){
plist aux = create(0);
aux=a;
	if(aux!=NULL){
		printf("%d ||", a->freq);
		print_arv1(a->left);
		print_arv1(a->right);
	}
}

plist correction(plist a){
plist* aux=&a;
	if(a->left!=NULL&&a->right!=NULL){
		correction(a->left);
		correction(a->right);
	}
(*aux)->left=a->right;
(*aux)->right=a->left;
return a;
}

int main(){
plist l = create(6);	
l = inserirnormal(l,4, 'c');
l = inserirnormal(l,3, 'b');
l = inserirnormal(l,0, 'a');
l = inserirnormal(l,11, 'c');
l = inserirnormal(l,1, 'b');
l = inserirnormal(l,-5, 'a');
int menor = menorelemento(l);
printf("menor %d\n", menor);
print_list(l);


return 0;
}

/*
// Prints huffman codes from the root of Huffman Tree. 
// It uses arr[] to store codes 
void printCodes(struct MinHeapNode* root, int arr[], int top) 
  
{ 
  
    // Assign 0 to left edge and recur 
    if (root->left) { 
  
        arr[top] = 0; 
        printCodes(root->left, arr, top + 1); 
    } 
  
    // Assign 1 to right edge and recur 
    if (root->right) { 
  
        arr[top] = 1; 
        printCodes(root->right, arr, top + 1); 
    } 
  
    // If this is a leaf node, then 
    // it contains one of the input 
    // characters, print the character 
    // and its code from arr[] 
    if (isLeaf(root)) { 
  
        printf("%c: ", root->data); 
        printArr(arr, top); 
    } 
} */
