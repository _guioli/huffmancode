#include "arvbin.h"
pnode allocation(){
	pnode aux= (pnode) malloc(sizeof(node));
	aux->left=NULL;
	aux->right=NULL;
	aux->freq=0;
	aux->symbol=-1;
return aux;
}

pnode dad(pnode lson, pnode rson){
	pnode aux =  allocation();
	aux->left=lson;
	aux->right=rson;
	aux->freq= (lson->freq)+(rson->freq);
return aux;
}

int empty_arv(pnode a){
	return (a==NULL);
}

pnode free_arv(pnode a){
	if(!empty_arv(a)){
		free_arv(a->left);
		free_arv(a->right);
		free_arv(a);
	}
return NULL;
}

int search_number(pnode a, int num){
	if(!empty_arv(a)){
		return a->freq==num||search_number(a->left, num)||search_number(a->right,num);
	}
	else
		return 0;
}

void print_arv(pnode a){
	if(!empty_arv(a)){
		printf("%d ", a->freq);
		print_arv(a->left);
		print_arv(a->right);
	}
}
/*
void imprime_inordem(pnode a)  //in_ordem
{
   if(!empty_arv(a))
   {
       imprime_inordem(a->left);
       printf("%d ", a->freq);
       imprime_inordem(a->right);
   }

}
*/
void bin_insert(node **r,int num)
{
   node *novo_no;

   novo_no = (node *)malloc(sizeof(node));
   if(*r == NULL)        //verifica se a raiz da arvore esta vazia
   {
       novo_no->freq = num;
       novo_no->left = NULL;
       novo_no->right = NULL;

       *r = novo_no;
	
   }
	
   else
   {
       if(((*r)->freq) < num)
       {

           bin_insert(&(*r)->right,num);
       }
       else
       {
           bin_insert(&(*r)->left,num);

       }
   }

}

/*
int main(){
pnode a,b,c;
//esse passo é importante
a= allocation();
pnode* dad= &a;
	
for(int i=0;i <15;i++){
	bin_insert(dad, i);
}	

//print_arv(*dad);
//imprime_inordem(*dad) ;
return 0;
}
*/





