#include <stdio.h>
#include <stdlib.h>
#include "arvbin.h"

typedef struct list{
node leaf;
struct list* next;
struct list* left;
struct list* right;
int tam;
int freq;
int symbol;
}list;


typedef list* plist;

plist create(int freq);
plist allocation_list();
plist create_empty();
int empty_list(plist l);
plist remove_list(plist l, int pos);
plist inserir(plist p, plist node, char c);
plist huffman_begin(plist l);
void print_list(plist l);
void print_arv1(plist a);
