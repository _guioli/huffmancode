#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int symbol;
	int freq;
	struct node* left;
	struct node* right;
}node;

typedef node* pnode;
pnode allocation();
pnode dad(pnode lson, pnode rson);
int empty_arv(pnode a);
pnode free_arv(pnode a);
int search_number(pnode a, int num);
void print_arv(pnode a);
void bin_insert(node **r,int num);
//void imprime_inordem(pnode a);
int imprime_pre_ordem(pnode arvore);


